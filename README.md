 # RecyclerViewPager

## 简介
> RecyclerViewPager是一个支持自定义左右翻页切换效果、上下翻页切换效果、类似Material风格的容器组件。

 ![效果展示](screenshot/RecyclerViewpager.gif)


## 下载安装
```shell
ohpm install @ohos/recyclerviewpager
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md )


## 使用说明

### singleFlingPager的使用
1. 导入
 ```
   import { singleFlingPager } from "@ohos/recyclerviewpager"
 ```

2. 传入自定义布局
 ```xml 
 自定义方法specificParam中传入自己的自定义布局
@Builder specificParam(item) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + item.i).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft, right: this.marginRight })
      .width(this.Containderwidth)
      .height('90%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 + this.offsetX / this.ScreenOffset : 1 - this.offsetX / this.ScreenOffset
      })
      .offset({x:'1%'})
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft, right: this.marginRight })
      .width(this.Containderwidth)
      .height('80%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 - this.offsetX / this.ScreenOffset : 1 + this.offsetX / this.ScreenOffset
      })
      .offset({x:'1%'})
    }
  }
 ```

3. 将布局传入容器内
 ```
    build() {
    Column() {
      Flex({ direction: FlexDirection.Column }) {
        singleFlingPager(
          {
            arr: this.arr,
            offsetX: $offsetX,
            index: $index,
            marginLeft: this.marginLeft,
            marginRight: this.marginRight,
            Containderwidth: this.Containderwidth,
            ContainderHeight: this.ContainderHeight,
            content: (item) => {
              this.specificParam(item)
            }
          }
        )
      }
    }
  }
 ```

### verticalViewPager的使用
1. 导入
 ```
   import { verticalViewPager } from "@ohos/recyclerviewpager"
 ```

2. 传入自定义布局
 ```xml 
 自定义方法specificParam中传入自己的自定义布局
    @Builder specificParam(item) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + this.index).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.topSpaced, bottom: this.topSpaced /*, left: this.topSpaced, right: this.topSpaced */
      })
      .width('100%')
      .height(this.ContainderHeight)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 + this.offsetY / this.ScreenOffset : 1 - this.offsetY / this.ScreenOffset,
        y: 1
      })
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ top: this.topSpaced, bottom: this.topSpaced, left: this.topSpaced, right: this.topSpaced })
      .width('90%')
      .height(this.ContainderHeight)
      .backgroundColor("#273238")
      .scale({
        x: this.offsetY < 0 ? 1 - this.offsetY / this.ScreenOffset : 1 + this.offsetY / this.ScreenOffset,
        y: 1
      })
    }

  }
 ```


3. 将布局传入容器内
 ```
    build() {
    Column() {
      Flex() {
        verticalViewPager({
          arr: this.arr,
          offsetY: $offsetY,
          index: $index,
          marginTop: this.topSpaced,
          marginBottom: this.topSpaced,
          ContainderWidth: this.ContainderWidth,
          ContainderHeight: this.ContainderHeight,
          content: (item) => {
            this.specificParam(item)
          }
        })
      }
    }
  }
 ```

### singleFlingPagerSelect的使用
1. 导入
 ```
   import { singleFlingPagerSelect } from "@ohos/recyclerviewpager"
 ```

2. 传入自定义布局
 ```
 自定义方法specificParam中传入自己的自定义布局
@Builder specificParam(item) {

    if (item.i == this.index) {
      Flex() {
        Text("item=" + item.i).fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft,right: this.marginRight })
      .width(this.Containderwidth)
      .height('90%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 + this.offsetX / this.ScreenOffset : 1 - this.offsetX / this.ScreenOffset
      })
    } else {
      Flex() {
        Text("item").fontSize(this.fontSize).fontColor("#e5e1e1")
      }
      .margin({ left: this.marginLeft,right: this.marginRight })
      .width(this.Containderwidth)
      .height('80%')
      .backgroundColor("#273238")
      .scale({
        x: 1,
        y: this.offsetX < 0 ? 1 - this.offsetX / this.ScreenOffset : 1 + this.offsetX / this.ScreenOffset
      })
    }

  }
 ```

3. 将布局传入容器内
 ```
    build() {
      Flex() {
        singleFlingPagerSelect({
          arr: this.arr,
          offsetX: $offsetX,
          index: $index,
          marginLeft: this.marginLeft,
          marginRight: this.marginRight,
          Containderwidth: this.Containderwidth,
          ContainderHeight: this.ContainderHeight,
          content: (item) => {
            this.specificParam(item)
          }
        })
      )}
    }
 ```

## 属性说明

### singleFlingPager属性说明
```xml 
arr: 页面文本内容,
offsetX: 页面滑动偏移,
index: 当前页面索引值,
marginLeft: 页面左边距,
marginRight: 页面右边距,
Containderwidth: 页面宽度
ContainderHeight: 页面高度
content: 容器内布局
```

### verticalViewPager属性说明
```xml 
arr: 页面文本内容,
offsetY: 页面滑动偏移,
index: 当前页面索引值,
marginTop: 页面顶边距,
marginBottomt: 页面下边距,
ContainderWidth: 页面宽度,
ContainderHeight: 页面高度,
content: 容器内布局
```

### singleFlingPagerSelect属性说明
```xml 
arr: 页面文本内容,
offsetX: 页面滑动偏移,
index: 当前页面索引值,
marginLeft: 页面左边距,
marginRight: 页面右边距,
Containderwidth: 页面宽度
ContainderHeight: 页面高度
content: 容器内布局
```


## 约束与限制

在下述版本验证通过：
- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK: API11 (4.1.0.36)

## 目录结构
````
|---- RecyclerViewPager 
|     |---- entry  # 示例代码文件夹
|     |---- library  # RecyclerViewPager库文件夹
|         |----src
|             |----main
|                     |----components
|                         |----materialContainderTop.ets #material风格实现
|                         |----verticalViewPager.ets #上下翻页效果实现
|         |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/RecyclerViewPager/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/RecyclerViewPager/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/RecyclerViewPager/blob/master/LICENSE) ，请自由地享受和参与开源。



